# Revision history for fcf-family

[Current version](https://gitlab.com/lysxia/fcf-family/-/blob/main/fcf-family/CHANGELOG.md)

## 0.2.0.2 - 2024-01-24

- Compatibility up to GHC 9.12.1

## 0.2.0.1

- Compatibility with GHC 9.8.1

## 0.2.0.0

- Change `fcfify` to deduplicate calls with the same argument.
- Add `fcfify'` with the old stateless behavior.
- Add `fcfifySkip`.

## 0.1.0.0 -- 2023-01-23

* First version. Released on an unsuspecting world.
