{-# LANGUAGE CPP, DataKinds, NoStarIsType, PolyKinds, TemplateHaskell, TypeFamilies #-}
{-# OPTIONS_GHC -Wno-orphans #-}

-- | First-class promotion of type families in /base/.
module Fcf.Base () where

import Data.Type.Bool
import Data.Type.Equality
import GHC.Generics (Rep)
import GHC.TypeLits
#if MIN_VERSION_base(4,16,0)
import Data.Type.Ord
#endif
#if MIN_VERSION_base(4,17,0)
import GHC.TypeError
#endif

import Fcf.Family.TH (fcfify)

-- Bool
fcfify ''(||)
fcfify ''(&&)
fcfify ''If
fcfify ''Not

-- Equality
fcfify ''(==)

-- Generics
fcfify ''Rep

-- TypeError
fcfify ''TypeError

-- TypeLits
fcfify ''AppendSymbol
fcfify ''CmpSymbol

-- TypeNats
fcfify ''(+)
fcfify ''(-)
fcfify ''(*)
fcfify ''Div
fcfify ''Mod
fcfify ''Log2
fcfify ''CmpNat

#if MIN_VERSION_base(4,16,0)
fcfify ''Compare
fcfify ''OrdCond
fcfify ''ConsSymbol
fcfify ''UnconsSymbol
fcfify ''CharToNat
fcfify ''NatToChar
fcfify ''CmpChar
#endif
#if MIN_VERSION_base(4,17,0)
fcfify ''Assert
#endif
