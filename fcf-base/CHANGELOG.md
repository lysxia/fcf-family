# Revision history for fcf-base

## 0.1.0.1 -- 2024-01-24

- Silence warnings about orphans

## 0.1.0.0 -- 2023-01-23

- First version. Released on an unsuspecting world.
