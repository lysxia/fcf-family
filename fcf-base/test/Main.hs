{-# LANGUAGE AllowAmbiguousTypes, DataKinds, PolyKinds, ScopedTypeVariables, TemplateHaskell, TypeApplications, TypeFamilies, TypeOperators #-}

module Main where

import Data.Type.Bool (If)
import Data.Type.Equality (type (==))
import GHC.TypeNats (type (+))

import Fcf.Family (Eval)
import Fcf.Family.TH (applyFamily)
import Fcf.Base ()

equals :: forall a b. (a ~ b) => IO ()
equals = pure ()

main :: IO ()
main = do
  equals @(Eval $(applyFamily ''(+) [ [t|3|] , [t|4|] ])) @7
  equals @(Eval $(applyFamily ''If [ [t|'True|] , [t|Int|] , [t|()|] ])) @Int
  equals @(Eval $(applyFamily ''(==) [ [t|()|] , [t|Int|] ])) @'False
