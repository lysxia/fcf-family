# Family of families

This library extends [first-class-families](https://hackage.haskell.org/package/first-class-families)
with a new way to promote existing type families.

- [*fcf-family*](./fcf-family): core package, start here.
- [*fcf-base*](./fcf-base): instances for base.
